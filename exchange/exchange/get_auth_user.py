from rest_framework.authtoken.models import Token
from user.models import UserProfile
from django.contrib.auth.models import User


def getuser(token: Token):
    [], token_string = token.split("Token ")
    user = Token.objects.get(key=token_string).user
    userprofile = UserProfile.objects.get(userid=user)
    return userprofile
