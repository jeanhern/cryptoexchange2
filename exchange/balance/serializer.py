from rest_framework.serializers import ModelSerializer
from .models import Balance


class BalanceSerializer(ModelSerializer):
    class Meta:
        model = Balance
        depth = 1
        fields = ('id', 'balance', 'blocked_balance', 'currency', 'user')
