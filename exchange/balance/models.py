from django.db import models
from currency.models import Currency
from user.models import UserProfile
from django.utils import timezone


class Balance(models.Model):
    balance = models.DecimalField(max_digits=8, decimal_places=6)
    blocked_balance = models.DecimalField(max_digits=8, decimal_places=6)
    currency = models.ForeignKey(Currency, related_name='balance_currency', on_delete=None)
    user = models.ForeignKey(UserProfile, related_name='user_balance', on_delete=None)

    def __str__(self):
        return self.currency.symbol + ": " + str(float(self.balance) - float(self.blocked_balance)) + " - " \
               + self.user.username

