from django.core.cache import cache
from .models import Offer, Transfer, Trade
from balance.models import Balance

def offer_name(offer:Offer):
    return offer.offer_currency.symbol + "-" + offer.wanted_currency.symbol


def search_name(offer: Offer):
    return offer.wanted_currency.symbol + "-" + offer.offer_currency.symbol


def initialize_offer_list(offer: Offer):
    cache.set(offer_name(offer), {
        str(offer.price): [{
            "amount": offer.amount,
            "user": offer.user,
            "id": offer.pk
        }]
    }, timeout=None)


def add_offer(offer: Offer):
    name = offer_name(offer)
    if cache.ttl(name) == 0 or len(cache.get(name)) == 0:
        initialize_offer_list(offer)
    else:
        offers = cache.get(name)
        if str(offer.price) not in offers.keys():
            offers[str(offer.price)] = []
        offers[str(offer.price)].append({
            "amount": offer.amount,
            "user": offer.user,
            "id": offer.pk
        })
        cache.set(name, offers, timeout=None)


def manage_pay(user, have, want,  give, receive):
    give_balance = Balance.objects.get(user=user, currency=have)
    give_balance.blocked_balance = float(give_balance.blocked_balance) - give
    give_balance.balance = float(give_balance.balance) - give
    give_balance.save()
    receive_balance = Balance.objects.get(user=user, currency=want)
    receive_balance.balance = float(receive_balance.balance) + receive
    receive_balance.save()


def manage_trade(offer: Offer, sell_offer: Offer, received, spend):
    spend_transfer = Transfer(
        from_balance=Balance.objects.get(user=offer.user, currency=offer.offer_currency
                                 ),
        to_balance=Balance.objects.get(user=sell_offer.user, currency=offer.offer_currency),
        amount=spend,
        currency=offer.offer_currency
    )
    receive_transfer = Transfer(
        from_balance=Balance.objects.get(user=sell_offer.user, currency=offer.wanted_currency),
        to_balance=Balance.objects.get(user=offer.user, currency=offer.wanted_currency),
        amount=received,
        currency=offer.wanted_currency
    )
    spend_transfer.save()
    receive_transfer.save()
    trade = Trade(
        initial_transfer=spend_transfer,
        final_transfer=receive_transfer
    )
    trade.save()
    manage_pay(offer.user, offer.offer_currency, offer.wanted_currency, spend, received)
    manage_pay(sell_offer.user, offer.wanted_currency, offer.offer_currency, received, spend)


# @celery.task
def match_offer(offer: Offer):
    search = search_name(offer)
    if cache.ttl(search) == 0 or len(cache.get(search)) == 0:
        add_offer(offer)
    else:
        offers = cache.get(search)
        prices = [price for price in offers.keys() if float(price) <= 1 / offer.price]
        total_spend = 0
        remaining = float(offer.amount) * float(offer.price)
        unfinished_offers = {price: offers[price] for price in offers.keys() if float(price) > 1 / offer.price}
        for price in prices:
            sellers = offers[price]
            for seller in sellers:
                sell_offer = Offer.objects.get(pk=seller["id"])
                if sell_offer.user != offer.user:
                    if float(sell_offer.amount) > remaining:
                        received = remaining
                        sell_offer.amount = float(sell_offer.amount) - received
                        sell_offer.total_spend = float(sell_offer.total_spend) + received
                        spend = remaining * float(price)
                        total_spend += spend
                        remaining = 0
                        if price not in unfinished_offers.keys():
                            unfinished_offers[price] = []
                        unfinished_offers[price].append(seller)
                    else:
                        received = float(sell_offer.amount)
                        remaining -= received
                        sell_offer.total_spend = float(sell_offer.total_spend) + received
                        spend = float(sell_offer.amount) * float(price)
                        total_spend += spend
                        sell_offer.amount = 0
                        sell_offer.status = "complete"
                    sell_offer.save()
                    manage_trade(offer, sell_offer, received, spend)
                else:
                    if price not in unfinished_offers.keys():
                        unfinished_offers[price] = []
                    unfinished_offers[price].append(seller)
                if remaining == 0:
                    break
            if remaining == 0:
                break
        offer.amount = float(offer.amount) - total_spend
        offer.total_spend = total_spend
        if remaining == 0:
            offer.status = "complete"
        offer.save()
        if offer.status == "wait":
            add_offer(offer)
        cache.set(search, unfinished_offers, timeout=None)
