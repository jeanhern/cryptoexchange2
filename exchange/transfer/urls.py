from django.urls import path
from .views import CreateOfferView, CancelOfferView, OfferView, OfferByCurrencyView, TransferView, PricesView


urlpatterns = [
    path('offer/create', CreateOfferView.as_view(), name='createOffer'),
    path('offer/cancel', CancelOfferView.as_view(), name='cancelOffer'),
    path('offer', OfferView.as_view(), name='getOffers'),
    path('offer/bycurrency', OfferByCurrencyView.as_view(), name='getOfferByCurrency'),
    path('get', TransferView.as_view(), name='getTransfers'),
    path('prices', PricesView.as_view(), name='getPrices')
]
