from .models import Offer, Transfer, Trade
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers


class OfferSerializer(ModelSerializer):
    wanted_currency = serializers.ReadOnlyField(source='wanted_currency.symbol')
    offer_currency = serializers.ReadOnlyField(source='offer_currency.symbol')

    class Meta:
        model = Offer
        depth = 1
        fields = (
                  'id',
                  'amount',
                  'initial_amount',
                  'total_spend',
                  'offer_currency',
                  'price',
                  'wanted_currency',
                  'status',
                  'datetime',
                  )


class TransferSerializer(ModelSerializer):
    currency_name = serializers.ReadOnlyField(source='currency.symbol')

    class Meta:
        model = Transfer
        fields = (
                  'amount',
                  'currency_name',
                  'datetime'
                  )


class TradeSerializer(ModelSerializer):
    class Meta:
        model = Trade
        fields = ('initial_transfer', 'final_transfer')
