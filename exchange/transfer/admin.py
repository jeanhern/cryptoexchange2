from django.contrib import admin
from .models import Transfer, Offer, Trade
# Register your models here.
admin.site.register(Transfer)
admin.site.register(Offer)
admin.site.register(Trade)
