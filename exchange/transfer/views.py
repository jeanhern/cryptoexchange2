from rest_framework.views import APIView
from rest_framework.response import Response
from user.models import UserProfile
from .models import Balance, Transfer, Offer, Trade
from currency.models import Currency
from .serializer import TransferSerializer, OfferSerializer
from .tasks import match_offer
from exchange.get_auth_user import getuser
from django.utils import timezone
from django.db.models import Q
import datetime


def userdoesnotexist(user):
    return {"error": "404 user " + user + " does not exist"}


def balancedoesnotexist():
    return {"error": "404 balance does not exist"}


def offerdoesnotexist(user):
    return {"error": "404 user " + user + " does not have standing offers"}


class CreateOfferView(APIView):

    def post(self, request):
        body = request.data
        try:
            username = getuser(request.headers["Authorization"]).username
            balance = Balance.objects.get(user__username=username,
                                          currency__symbol=body["offer_currency"])
            if float(balance.balance) - float(balance.blocked_balance) >= float(body["amount"]):
                balance.blocked_balance = float(balance.blocked_balance) + float(body["amount"])
                balance.save()
                offer = Offer(
                    initial_amount=float(body["amount"]),
                    amount=float(body["amount"]),
                    offer_currency=Currency.objects.get(symbol=body["offer_currency"]),
                    price=float(body["price"]),
                    wanted_currency=Currency.objects.get(symbol=body["wanted_currency"]),
                    user=UserProfile.objects.get(username=username)
                )
                offer.save()
                match_offer(offer)
                return Response(OfferSerializer(offer).data)
            else:
                return Response({"error": "Balance is insufficient to perform the requested offer"})
        except Balance.DoesNotExist:
            return Response(balancedoesnotexist())


class CancelOfferView(APIView):

    def post(self, request):
        body = request.data
        user = getuser(request.headers["Authorization"])
        try:
            offer = Offer.objects.get(pk=body["offerId"], user=user)
            balance = Balance.objects.get(currency=offer.offer_currency, user=user)
            balance.blocked_balance = float(balance.blocked_balance) - float(offer.amount)
            offer.status = 'cancel'
            offer.save()
            balance.save()
            return Response({"successs": "the offer has been cancelled"})
        except Offer.DoesNotExist:
            return Response(offerdoesnotexist(user.username))


class OfferView(APIView):

    def get(self, request):
        try:
            username = getuser(request.headers["Authorization"]).username
            offers = Offer.objects.filter(user__username=username)
            return Response(OfferSerializer(offers, many=True).data)
        except Offer.DoesNotExist:
            return Response(offerdoesnotexist(username))


class OfferByCurrencyView(APIView):
    def getOffers(self, user, offer_currency, wanted_currency):
        try:
            offers = Offer.objects.filter(
                offer_currency__symbol=offer_currency,
                wanted_currency__symbol=wanted_currency,
                status="wait"
            ).exclude(user__username=user)
            return Response(OfferSerializer(offers, many=True).data)
        except Offer.DoesNotExist:
            return Response({"error": "404 there are no offers for the given currencies"})

    def get(self, request):
        return self.getOffers(
            getuser(request.headers["Authorization"]).username,
            request.query_params.get("offer_currency"),
            request.query_params.get("wanted_currency")
        )

    def post(self, request):
        body = request.POST or request.data
        return self.getOffers(
            getuser(request.headers["Authorization"]).username,
            body["offer_currency"],
            body["wanted_currency"]
        )


class TransferView(APIView):
    def get(self, request):
        try:
            username = getuser(request.headers["Authorization"]).username
            received_transfers = Transfer.objects.filter(to_balance__user__username=username)
        except Transfer.DoesNotExist:
            received_transfers = []
        try:
            sent_transfers = Transfer.objects.filter(from_balance__user__username=username)
        except Transfer.DoesNotExist:
            sent_transfers = []
        return Response({
                "received": TransferSerializer(received_transfers, many=True).data,
                "sent": TransferSerializer(sent_transfers, many=True).data
            })


def getprices(transactioninfos):
    prices = []
    for info in transactioninfos:
        prices.append([info["date"], float(info["want"]) / float(info["offer"])])
    print(prices)
    return prices


class PricesView(APIView):
    def calculateprice(self, currency1, currency2):
        start_date = timezone.now() - datetime.timedelta(days=30)
        try:
            date_trades = Trade.objects.filter(initial_transfer__datetime__gte=start_date)
            buy_trades = date_trades.filter(initial_transfer__currency=currency1, final_transfer__currency=currency2)
            buy_infos = [{
                "offer": trade.initial_transfer.amount,
                "want": trade.final_transfer.amount,
                "date": trade.initial_transfer.datetime.timestamp()
            } for trade in buy_trades]
            sell_trades = date_trades.filter(initial_transfer__currency=currency2, final_transfer__currency=currency1)
            sell_infos = [{
                "offer": trade.final_transfer.amount,
                "want": trade.initial_transfer.amount,
                "date": trade.initial_transfer.datetime.timestamp()
            } for trade in sell_trades]
            buy_prices = getprices(buy_infos)
            sell_prices = getprices(sell_infos)
            return {
                "buy": buy_prices,
                "sell": sell_prices
            }
        except Trade.doesNotExist:
            return Response({"error": "there were no trades done in the given period"})
        return prices

    def get(self, request):
        try:
            currency1 = Currency.objects.get(symbol=request.query_params.get("currency1"))
            currency2 = Currency.objects.get(symbol=request.query_params.get("currency2"))
            return Response(self.calculateprice(currency1, currency2))
        except Currency.DoesNotExist:
            return Response({"error": "currency does not exist"})

