django==2.2.6
redis==3.3.9
djangorestframework==3.10.3
psycopg2==2.8.3
django-redis==4.10.0
gunicorn==19.9.0
django-cors-headers