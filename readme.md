to run

docker-compose build
docker-compose up



default users:
    admin/admin
    clouding/1598741jh
    test2/1598741jh

the offers logic is based on the idea that each user presents their offer as selling the <amount> of the <offered_currency> for <price> of the <want_currency>(EACH)
so in order to make offers match, lets say user clouding makes an offer: Offer 2BTC  PRICE 4ETH, and you want to take that offer with test2, then you need to make an of ETH at a number equal or lower than 1/PRICE like OFFER 3ETH at 0.5BTC    -> (if clouding is selling 4BTC at 2ETH each then the price of a BTC to match that ETH price to pay him what he wants is 0.5BTC at most) 