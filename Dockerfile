FROM python:3.6
ENV PYTHONUNBUFFERED 1
RUN mkdir /config
ADD /exchange/requirements.txt /config/
RUN pip install -r /config/requirements.txt
RUN mkdir /src;
WORKDIR /src