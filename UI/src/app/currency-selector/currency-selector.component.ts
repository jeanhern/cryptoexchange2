import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BalanceService } from '../services/balance.service';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'app-currency-selector',
  templateUrl: './currency-selector.component.html',
  styleUrls: ['./currency-selector.component.scss']
})
export class CurrencySelectorComponent implements OnInit {
  currencies: any[] = [];
  form: FormGroup;
  isLoading = false;
  @Input() showNumberFields = false;
  @Output() currencyChange = new EventEmitter();

  constructor(private balance: BalanceService, private fb: FormBuilder) {}

  ngOnInit() {
    this.isLoading = true;
    this.balance.getCurrencies().subscribe(response => {
      this.currencies = response;
      this.isLoading = false;
    });
    this.form = this.fb.group({
      want_currency: ['', Validators.required],
      amount: [''],
      offered_currency: ['', Validators.required],
      price: ['']
    });
    this.form.valueChanges.subscribe(value => {
      if (this.form.valid) {
        this.currencyChange.emit({ ...value, valid: this.validateForm(this.form) });
      }
    });
  }

  private checkNumber(n: any) {
    return n === '' || isNaN(n) || (n.split('.')[1] && n.split('.')[1].length > 5) || Number(n) >= 100;
  }

  private validateForm(form: FormGroup) {
    const values = form.value;
    const errors = {
      want_currency: false,
      offered_currency: false,
      price: false,
      amount: false
    };
    if (values.want_currency === values.offered_currency) {
      errors.want_currency = true;
      errors.offered_currency = true;
    }
    if (this.checkNumber(values.amount)) {
      errors.amount = true;
    }
    if (this.checkNumber(values.price)) {
      errors.price = true;
    }
    if (errors.want_currency || errors.offered_currency || errors.price || errors.amount) {
      return errors;
    }
    return null;
  }
}
