import { Component, OnInit } from '@angular/core';
import { TransferService } from '../services/transfer.service';
import * as moment from 'moment';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements OnInit {
  isLoading = false;
  offers: any[] = [];

  constructor(private transfer: TransferService) {}

  ngOnInit() {
    
    this.isLoading = true;
    this.transfer.getOffers().subscribe(response => {
      this.offers = response.sort((offer1: any, offer2: any) => {
        return offer1.datetime < offer2.datetime ? -1 : 1;
      });
      this.isLoading = false;
    });
  }

  formatDate(date: string) {
    return moment(date).format('YYYY-MM-DD');
  }

  cancelOffer(offer: any) {
    this.isLoading = true;
    if (offer.status === 'wait') {
      this.transfer.cancelOffer(offer.id).subscribe(response => {
        window.location.reload();
      })
    }
  }
}
