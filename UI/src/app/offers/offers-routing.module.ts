import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { OffersComponent } from './offers.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'offers', component: OffersComponent, data: { title: extract('offers') } }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class OffersRoutingModule {}
