import { Component, OnInit } from '@angular/core';
import { TransferService } from '../services/transfer.service';
import { environment } from '@env/environment';
import * as moment from 'moment';

@Component({
  selector: 'app-transfers',
  templateUrl: './transfers.component.html',
  styleUrls: ['./transfers.component.scss']
})
export class TransfersComponent implements OnInit {
  version: string | null = environment.version;
  isLoading = false;
  transfers: { received: any[]; sent: any[] } = { received: [], sent: [] };

  constructor(private transfer: TransferService) {}

  ngOnInit() {
    this.isLoading = true;
    this.transfer.getTransfers().subscribe(response => {
      this.transfers = response;
    });
  }

  formatDate(date: string) {
    return moment(date).format('YYYY-MM-DD');
  }
}
