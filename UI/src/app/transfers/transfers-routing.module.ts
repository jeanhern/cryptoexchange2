import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { Shell } from '@app/shell/shell.service';
import { TransfersComponent } from './transfers.component';

const routes: Routes = [
  Shell.childRoutes([{ path: 'transfers', component: TransfersComponent, data: { title: extract('transfers') } }])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class TransfersRoutingModule {}
