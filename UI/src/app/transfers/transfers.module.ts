import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/material.module';
import { TransfersRoutingModule } from './transfers-routing.module';
import { TransfersComponent } from './transfers.component';

@NgModule({
  imports: [CommonModule, TranslateModule, FlexLayoutModule, MaterialModule, TransfersRoutingModule],
  declarations: [TransfersComponent]
})
export class TransfersModule {}
