import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { CurrencySelectorModule } from '../currency-selector/currency-selector.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OfferListComponent } from './offer-list/offer-list.component';
import { HighchartsChartModule } from 'highcharts-angular';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    MatSnackBarModule,
    CurrencySelectorModule,
    HighchartsChartModule,
    HomeRoutingModule
  ],
  declarations: [HomeComponent, OfferListComponent]
})
export class HomeModule {}
