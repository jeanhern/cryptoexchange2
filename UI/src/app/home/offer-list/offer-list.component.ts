import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-offer-list',
  templateUrl: './offer-list.component.html',
  styleUrls: ['./offer-list.component.scss']
})
export class OfferListComponent implements OnInit, OnChanges {
  showLists = false;
  @Input() offers: any[] = [];
  @Input() label = 'offers';
  @Input() wantCurrency: string;
  @Input() haveCurrency: string;
  @Input() isLoading = false;
  @Input() sortOrder: string;
  listOffers: any = {};
  objectkeys = Object.keys;

  constructor() {}

  ngOnInit() {}

  formatDate(date: string) {
    return moment(date).format('YYYY-MM-DD');
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.offers && changes.offers.currentValue && Object.keys(changes.offers.currentValue).length > 0) {
      this.listOffers = {};
      this.showLists = true;
      this.offers.forEach(offer => {
        if (this.listOffers[offer.price]) {
          this.listOffers[offer.price].sellers += 1;
          this.listOffers[offer.price].total += Number(offer.amount);
        } else {
          this.listOffers[offer.price] = {
            sellers: 1,
            total: Number(offer.amount)
          };
        }
      });
    } else if (!changes.isLoading) {
      this.listOffers = {};
      this.showLists = false;
    }
  }

  sortOffers() {
    const prices = Object.keys(this.listOffers);
    if (this.sortOrder === 'ASC') {
      return prices.sort((elem, elem2) => Number(elem) - Number(elem2));
    } else {
      return prices.sort((elem, elem2) => Number(elem2) - Number(elem));
    }
  }
}
