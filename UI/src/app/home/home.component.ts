import { Component, OnInit } from '@angular/core';
import { TransferService } from '../services/transfer.service';
import { MatSnackBar } from '@angular/material';
import { forkJoin } from 'rxjs';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  chartCallback: any;
  chart: any;
  updateFlag = false;
  isLoading = true;
  selectedCurrencies: any = {};
  buy_from_offers: any[] = [];
  sell_as_offers: any[] = [];
  showChart = false;
  Highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'line',
      width: 900
    },
    title: {
      text: ''
    },
    xAxis: {
      type: 'datetime'
    },
    yAxis: {
      allowDecimals: true,
      type: 'number'
    },
    legend: {
      layout: 'vertical',
      borderwidth: 1
    },
    credits: {
      enabled: false
    },
    series: [{}]
  };

  constructor(private transfer: TransferService, public snackBar: MatSnackBar) {
    const self = this;

    this.chartCallback = (chart: any) => {
      self.chart = chart;
    };
  }

  ngOnInit() {
    this.isLoading = false;
    this.chartOptions.series.pop();
  }

  selectCurrency(currency: any) {
    if (
      (currency.want_currency !== this.selectedCurrencies.want_currency ||
        currency.offered_currency !== this.selectedCurrencies.offered_currency) &&
      currency.want_currency !== currency.offered_currency
    ) {
      this.isLoading = true;
      this.buy_from_offers = [];
      this.sell_as_offers = [];
      forkJoin([
        this.transfer.getOffersByCurrency(currency.offered_currency, currency.want_currency),
        this.transfer.getOffersByCurrency(currency.want_currency, currency.offered_currency),
        this.transfer.getPrices(currency.want_currency, currency.offered_currency)
      ]).subscribe(([response1, response2, response3]) => {
        this.sell_as_offers = response1;
        this.buy_from_offers = response2;
        this.chartOptions.series = [{ name: 'buy', data: response3.buy }, { name: 'sell', data: response3.sell }];
        this.updateFlag = true;
        this.showChart = true;
        this.isLoading = false;
      });
    }
    this.selectedCurrencies = currency;
  }

  postOffer() {
    if (this.selectedCurrencies.valid === null) {
      this.isLoading = true;
      this.transfer
        .createOffer(
          this.selectedCurrencies.amount,
          this.selectedCurrencies.offered_currency,
          this.selectedCurrencies.want_currency,
          this.selectedCurrencies.price
        )
        .subscribe(response => {
          this.isLoading = false;
          this.snackBar.open('Offer Posted successfully, go to my offers tab to check status.');
        });
    }
  }
}
