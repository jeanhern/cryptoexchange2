import { HttpService } from '../core/http/http.service';
import { UrlService } from './urls.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {
  constructor(private http: HttpService, private urls: UrlService) {}

  getBalance(): Observable<any> {
    return this.http.get(this.urls.urls.getBalance);
  }

  addBalance(currency: string, amount: number): Observable<any> {
    return this.http.post(this.urls.urls.addBalance, {
      currency,
      amount
    });
  }

  getCurrencies(): Observable<any> {
    return this.http.get(this.urls.urls.getCurrencies);
  }
}
