import { HttpClient } from '@angular/common/http';
import { UrlService } from './urls.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransferService {
  constructor(private http: HttpClient, private urls: UrlService) {}

  getOffers(): Observable<any> {
    return this.http.get(this.urls.urls.getUserOffers);
  }

  getOffersByCurrency(offer_currency: string, wanted_currency: string): Observable<any> {
    return this.http.post(this.urls.urls.getOffersByCurrency, {
      offer_currency,
      wanted_currency
    });
  }

  getTransfers(): Observable<any> {
    return this.http.get(this.urls.urls.getUserTransfers);
  }

  createOffer(amount: number, offer_currency: string, wanted_currency: string, price: number): Observable<any> {
    return this.http.post(this.urls.urls.createOffer, {
      amount,
      wanted_currency,
      offer_currency,
      price
    });
  }

  cancelOffer(offerId: number): Observable<any> {
    return this.http.post(this.urls.urls.cancelOffer, {offerId});
  }

  getPrices(want_currency: string, offer_currency: string): Observable<any> {
    return this.http.get(
      this.urls.urls.getTradePrices + '?currency1=' + want_currency + '&currency2=' + offer_currency
    );
  }
}
