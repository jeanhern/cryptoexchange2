import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UrlService {
  urls = {
    login: 'user/login',
    getCurrencies: 'currency/',
    getBalance: 'balance/get',
    addBalance: 'balance/create',
    getUserOffers: 'transfer/offer',
    getOffersByCurrency: 'transfer/offer/bycurrency',
    createOffer: 'transfer/offer/create',
    cancelOffer: 'transfer/offer/cancel',
    getUserTransfers: 'transfer/get',
    getTradePrices: 'transfer/prices'
  };
}
